#!/bin/bash
cp ../04_shape/${1}_1.shapeit.phased.sample ${1}.sample
sed 's/1$/0/g' ${1}.sample >${1}.temp.sample
sed -i 's/2$/1/g' ${1}.temp.sample
cut -d " " -f 1,2,4,5,6,7 ${1}.sample >${1}.fam.temp
tail -n +3 ${1}.fam.temp >${1}.fam
rm ${1}.fam.temp
