#!/usr/bin/python

# toDosage v0.15
# Script for converting from two/three probabilities (IMPUTE2 output) to dosages
# Till Andlauer (till.andlauer@tum.de)

import argparse, logging, os, itertools, sys

missChar = "0"
refAllele = "0"
maleX = "2"
samplefile = ""
showProgress = True
twop = False
header = 5
rsID = 1
A1 = 3
A2 = 4
firstLine = False

def readFile(filename): # load file
	inFile = []
	with open(filename, "r") as read:
		for line in read:
			inFile.append(line.strip())
	return inFile

def loadSampleSex(filename): # load sex from sample file
	samples = readFile(samplefile)
	# samples = samples[2:(len(samples)+1)]
	samples = samples[2:len(samples)]
	sex = []
	for line in samples:
		line = line.strip().split(" ")
		sex.append(line[5])
	return sex

def recodeD(token, sampleSex):
	tokenOut = []
	sampleC = 0
	if refAllele=="0": # calc MAF
		frq = []
		for index, item in enumerate(token):
			if index%3==0:
				frq.append(2*float(token[index])+float(token[index+1]))
		maf = sum(frq)/(2*len(frq))

	for index, item in enumerate(token):
		if index%3==0:
			if token[index]=="0" and token[index+1]=="0" and token[index+2]=="0":
				token[index+1] = missChar
				token[index+2] = missChar
			if maleX=="1": # set dosage 2=1 in males
				if sampleSex[sampleC]=="1": # male
					if refAllele=="1":
						tokenOut.append(str(float(token[index])))
						maf = 0
					elif refAllele=="2":
						tokenOut.append(str(float(token[index+2])))
						maf = 1
					else: # refAllele=="0" / use MAF
						if maf<0.5:
							tokenOut.append(str(float(token[index])))
						else:
							tokenOut.append(str(float(token[index+2])))
				else: # female
					if refAllele=="1":
						tokenOut.append(str(2*float(token[index])+float(token[index+1])))
						maf = 0
					elif refAllele=="2":
						tokenOut.append(str(float(token[index+1])+2*float(token[index+2])))
						maf = 1
					else: # refAllele=="0" / use MAF
						if maf<0.5:
							tokenOut.append(str(2*float(token[index])+float(token[index+1])))
						else:
							tokenOut.append(str(float(token[index+1])+2*float(token[index+2])))
			else: # keep standard dosage in males
				if refAllele=="1":
					tokenOut.append(str(2*float(token[index])+float(token[index+1])))
					maf = 0
				elif refAllele=="2":
					tokenOut.append(str(float(token[index+1])+2*float(token[index+2])))
					maf = 1
				else: # refAllele=="0" / use MAF
					if maf<0.5:
						tokenOut.append(str(2*float(token[index])+float(token[index+1])))
					else:
						tokenOut.append(str(float(token[index+1])+2*float(token[index+2])))
			sampleC += 1

	return tokenOut, maf

def recode2P(token, sampleSex, firstLine):
	tokenOut = []
	sampleC = 0
	if refAllele=="0": # calc MAF
		if firstLine:
			maf = 0
		else:
			frq = []
			for index, item in enumerate(token):
				if index%2==0:
					frq.append(2*float(token[index])+float(token[index+1]))
			maf = sum(frq)/(2*len(frq))

	for index, item in enumerate(token):
		if index%2==0:
			if firstLine:
				tokenOut.append(token[index])
			elif refAllele=="1":
				tokenOut.append(str(2*float(token[index])+float(token[index+1])))
				maf = 0
			elif refAllele=="2":
				tokenOut.append(str(float(token[index+1])+2*(1-(float(token[index])+float(token[index+1])))))
				maf = 1
			else: # refAllele=="0" / use MAF
				if maf<0.5:
					tokenOut.append(str(2*float(token[index])+float(token[index+1])))
				else:
					tokenOut.append(str(float(token[index+1])+2*(1-(float(token[index])+float(token[index+1])))))
			sampleC += 1

	return tokenOut, maf

def processFile(infile, outname, sampleSex):
	result = []

	count = 0 # for progress bar
	barLength = 100

	firstLine = True
	for line in infile:
		line = line.strip().split(" ")
		if twop:
			cLine, maf = recode2P(line[header:], sampleSex, firstLine)
		else:
			cLine, maf = recodeD(line[header:], sampleSex)
		
		firstLine = False
		if maf<0.5: # switch alleles depending on MAF
			a1 = line[A1]
			a2 = line[A2]
		else:
			a1 = line[A2]
			a2 = line[A1]

		result.append(line[rsID]+" "+a1+" "+a2+" "+" ".join(cLine)+"\n")

		if showProgress:
			# progress bar
			count += 1
			progress = int(round(count/float(len(infile))*barLength))
			if progress%1==0:
				sys.stdout.write("\rConverting: [{0}] {1} % ".format("#"*progress + "-"*(barLength-progress), progress))
				sys.stdout.flush()
	sys.stdout.write("\n")

	logging.warning("\nWriting file {0:s}.dat...".format(outname)) # everything's done, save files
	if os.path.exists(outname+".dat"): # remove any previous file
		os.remove(outname+".dat")
	with open(outname+".dat", "a") as outfile:
		for item in result:
			outfile.write(item)

	logging.warning("Done.\n")
	return True

### MAIN ###
version="0.15"

print("")
from argparse import RawTextHelpFormatter
parser = argparse.ArgumentParser(formatter_class=RawTextHelpFormatter,description="DosageConverter v"+version+".\nConverts from three probabilities to dosage.\n",epilog="Happy converting!\n")
parser.add_argument("-i", metavar="input", help="Input file", required=True, dest="input")
parser.add_argument("-a", metavar="allele", help="Reference Allele, 0 = Minor Allele (default), 1 = Allele 1, 2 = Allele 2", dest="allele")
parser.add_argument("-p", metavar="progress", help="Show progress (0 = no, 1 = yes (default))", dest="progress", default="1")
parser.add_argument("-x", metavar="X", help="Coding of male dosage on X (2 = homozygous=2 (default), 1 = homozygous=1 (requires sample file))", dest="xmale", default="2")
parser.add_argument("-s", metavar="sample", help="Sample file (for option x=1)", dest="sample")
parser.add_argument("-hc", metavar="header", help="Number of header columns (default=5)", dest="header", default="5")
parser.add_argument("-c1", metavar="column_rs", help="Position of rsID (default=1)", dest="c1", default="1")
parser.add_argument("-c2", metavar="column_A1", help="Position of allele 1 code (default=3)", dest="c2", default="3")
parser.add_argument("-c3", metavar="column_A2", help="Position of allele 2 code (default=4)", dest="c3", default="4")
parser.add_argument("-r", metavar="ricopili", help="Process Ricopili files (two probabilities) (0 = no (default), 1 = yes)", dest="ricopili", default="0")
#parser.add_argument("-o", metavar="output", help="Base name of the output file", required=True, dest="output")
args = parser.parse_args()

if args.progress=="0":
	showProgress = False
args.output = args.input
if args.xmale=="1":
	args.output = args.output + "_maleD1"
if args.ricopili=="1":
	twop = True

# initialize log file and log output to screen
logging.basicConfig(filename=args.output+".log",filemode="w",format="%(message)s",level=logging.INFO)
console = logging.StreamHandler()
logging.getLogger("").addHandler(console)
console.setLevel(logging.WARNING) # display less details on screen

import time # get time
from time import localtime, strftime
starttime = strftime("%d.%m.%Y, %H:%M:%S", localtime())
start_seconds = time.time()

logging.warning("DosageConverter v{0:s}".format(version))
logging.warning("---------------------\n")
logging.warning("{0:s}".format(starttime))
logging.warning("Input file: {0:s}".format(args.input))
logging.warning("Converted file: {0:s}.dat".format(args.output))


header = int(args.header)
rsID = int(args.c1)
A1 = int(args.c2)
A2 = int(args.c3)
logging.warning("Number of header columns: {0:d}".format(header))
logging.warning("Position of rsID column: {0:d}".format(rsID))
logging.warning("Position of A1 column: {0:d}".format(A1))
logging.warning("Position of A2 column: {0:d}".format(A2))

if args.allele:
	refAllele = str(args.allele)
	if refAllele=="1":
		logging.warning("Reference for dosage: Allele 1")
	elif refAllele=="2":
		logging.warning("Reference for dosage: Allele 2")
	elif refAllele=="0":
		logging.warning("Reference for dosage: Minor allele")
	else:
		logging.warning("WARNING: Invalid value for the reference allele, switching to minor allele")
		refAllele = "0"
else: logging.warning("Reference for dosage: Minor allele")

if twop:
	logging.warning("Processing two probabilities per individual (ricopili mode)")
	header = 3
	rsID = 0
	A1 = 1
	A2 = 2

sampleSex = ""
if args.xmale:
	maleX = str(args.xmale)
	if maleX!="2":
		samplefile = args.sample
		if samplefile:
			if maleX=="1":
				logging.warning("Coding homozygous (minor) alleles on X in male samples as 1.")
				sampleSex = loadSampleSex(samplefile)
			else:
				logging.warning("Invalid option for male coding given, using standard dosage for males.")
				maleX="2"
		else:
			logging.error("No sample file given, aborting...")
			sys.exit()

logging.warning("\nReading genotypes {0:s}...".format(args.input))
genoFile = readFile(args.input) # load file

logging.warning("\nConverting {0:d} SNPs...".format(len(genoFile)))
processFile(genoFile, args.output, sampleSex)

endtime = strftime("%d.%m.%Y, %H:%M:%S", localtime())
logging.warning("{0:s}".format(endtime))
logging.warning("Running time: {0:d} seconds.".format(int(round(time.time()-start_seconds))))
print("")
