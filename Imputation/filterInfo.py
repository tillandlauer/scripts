#!/usr/bin/python

# filterInfo v0.1
# Till Andlauer
# till.andlauer@tum.de

import argparse, logging, os, re, sys

info_thresh = 0.9

def readFile(filename):
    contents = []
    with open(filename, "r") as read:
        for line in read:
            contents.append(line.strip())
    return contents

def qc(prefix):
    count_in = 0
    count_out = 0
    info_sum = []

    # load files, perform qc
    for i in range(22):
        chrom = str(i+1)
        logging.info("Processing Chromosome "+chrom+"...")
        info_read = prefix+"_"+chrom+".out_info"

        if os.path.exists(info_read):
            i_r = readFile(info_read)
            iter_i_r = iter(i_r)
            next(iter_i_r)

            for info in iter_i_r:
                count_in += 1
                info = info.split(" ")
                if float(info[4])<info_thresh:
                    # qc: line[4]=INFO, line[3]=FREQUENCY
                    count_out += 1
                    info_sum.append(" ".join(info)+"\n")

    # write files
    logging.info("Number of SNPs before QC: "+str(count_in))
    logging.info("Number of SNPs after QC: "+str(count_out))
    print("Writing files...")
    info_write = prefix+"_"+str(info_thresh)+".exclude"
    with open(info_write, "a") as i_w:
        for item in info_sum:
            i_w.write(item)

    return True

# MAIN
# process input arguments
version="0.1"

print("")
parser = argparse.ArgumentParser(description="infoFilter v"+version+". Generates a list of SNPs below INFO threshold.",epilog="Happy listing.")
parser.add_argument("-p", metavar="pattern", help="File name pattern", required=True, dest="pattern")
parser.add_argument("-i", metavar="info", help="Info score threshold (default: 0.9)", dest="info", default="0.9")
args = parser.parse_args()

prefix = args.pattern
info_thresh = float(args.info)

logging.basicConfig(filename=prefix+"_"+str(info_thresh)+".log",filemode="w",format="%(message)s",level=logging.INFO)
console = logging.StreamHandler()
logging.getLogger("").addHandler(console)

logging.info("infoFilter v%s" % version)
logging.info("------------\n")
logging.info("Input files:") 
logging.info(prefix+"_CHR.out_info")
logging.info("Output files:")
logging.info(prefix+"_"+str(info_thresh)+".exclude")
logging.info(prefix+"_"+str(info_thresh)+".log")
logging.info("INFO threshold is:")
logging.info("INFO >= "+str(info_thresh))

import time
start_time = time.time()

# run qc
logging.info("Processing chromosomes 1-22.\n")
qc(prefix)

logging.info("Running time: %s seconds.\n" % str(round(time.time()-start_time)))
print("")
