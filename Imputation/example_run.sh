#!/bin/bash

# 1: Cohort (imputation base name)
# 2: Chromosome

./createSample.sh ${1}
python info_qc.py -p ${1} -c ${2}
python toDosage.py -i ${1}_${2}.out
Rscript IMPUTE2_to_RDS.R ${1} ${2}
